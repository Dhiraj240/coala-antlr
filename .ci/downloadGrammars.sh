set -e
cd "${0%/*}"
mkdir grammars
curl https://raw.githubusercontent.com/antlr/grammars-v4/8024ba5cceff76c8c825a336a72ba2c60d5958b3/python3-py/Python3.g4 > grammars/Python3.g4
curl https://raw.githubusercontent.com/antlr/grammars-v4/master/xml/XMLLexer.g4 > grammars/XMLLexer.g4
curl https://raw.githubusercontent.com/antlr/grammars-v4/master/xml/XMLParser.g4 > grammars/XMLParser.g4
