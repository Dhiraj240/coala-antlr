set -e
cd "${0%/*}"
antlr4='java -jar /usr/local/lib/antlr-4.7.1-complete.jar'
$antlr4 -Dlanguage=Python3 grammars/Python3.g4 -visitor -Xexact-output-dir -o ./Python3/
$antlr4 -Dlanguage=Python3 grammars/XMLLexer.g4 grammars/XMLParser.g4 -visitor -Xexact-output-dir -o ./xml/
python generate_all_in_one.py
coala --bears PEP8Bear --files=../coantlib/coantparsers/*.py --apply-patches -I || true
