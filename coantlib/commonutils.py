"""Common utilities for coantlib."""

import antlr4

from coantlib.NodeData import NodeData


def flatten(lhs, rhs):
    """
    Flatten the values from input and group them aptly.

    This utility flattens the values from a tree format into
    more usable tuples of assignment.

    For e.g::

        (x, y) = ("var1", ["abc", "def"])

    The raw results are::

        lhs = (x, y)
        rhs = ("var1", ("abc", "def"))

    After flattening, this will be combined as::

        combined = [ (x, "var1"), (y, ("abc", "def")) ]
    """
    final = []
    for l, r in zip(lhs, rhs):
        if not isinstance(l, NodeData):
            subresult = flatten(l, r)
            final += subresult
        else:
            final.append((l, r))
    return final


def get_line_and_col(node):
    r"""
    Retrieve line and column numbers from an ANTLR ParseTree context.

    This little utility function retrieves line number and column number
    for a given node context by traversing to that node's left-most
    terminal node.

    For e.g Let's say a parse tree is::

           A
         /   \\
        B     C

    And node provided is ``A``, this function will traverse to ``B``, and from
    that terminal node, report line and column numbers that are useful and
    compatible with ``coala`` for diff generation.
    """
    while not isinstance(node, antlr4.tree.Tree.TerminalNode):
        node = node.getChild(0)
    line = node.getPayload().line
    col = node.getPayload().column+1
    return (line, col)
