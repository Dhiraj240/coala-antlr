"""Load Parse Tree of a file into memory."""

from antlr4 import InputStream, CommonTokenStream
from coalib.bearlib.languages.Language import Language
from coantlib.mapper import parser_map


class ASTLoader():
    """
    A class that provides static method for loading a ParseTree.

    It is required to have corresponding entrypoints defined in ``mapper.py``
    for this class to be able to detect the appropriate parser and lexer to
    use for parse-tree creation.
    """

    @staticmethod
    def loadFile(file, filename, lang='auto'):
        """
        Load file's AST into memory.

        :param file:        Contents of file as a tuple of strings.
        :param filename:    Qualified (absolute) name of file for which the
                            parse tree is being loaded.
        :param lang:        Determines the language of ``file``.
                            If set to ``auto``, then it will try to detect the
                            language using extension of ``filename``.
        """
        language_class = Language[lang]

        # The newline at end is due to limitations of ANTLR
        # grammar for Python, needs to be removed at upstream before
        # removal from the library
        contents = ''.join(file)
        if len(file) == 0 or contents[-1] != '\n':
            contents = contents + '\n'
        our_map = parser_map[language_class.__class__]

        lexer = our_map[0](InputStream(contents))
        tokenisedInputStream = CommonTokenStream(lexer)
        parser = our_map[1](tokenisedInputStream)
        entrypoint_func = getattr(parser, our_map[4])

        return parser, entrypoint_func
