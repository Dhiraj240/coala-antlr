from coantbears.python.PyPluralNamingBear import PyPluralNamingBear
from coalib.testing.LocalBearTestHelper import verify_local_bear

goodFileA = """
def func(abc, *args, **kwargs):
    pass
"""

goodFileB = """
def func():
    pass
"""

goodFileC = """
def func(*arguments, **keywordargs):
    pass
"""

badFileA = """
def func(*singularargument, **keywordargs):
    pass
"""

badFileB = """
def func(**keyword):
    pass
"""

singularSequencesBad = """xval = {k:v, k2:v2}
(singleAssignment,yval) = ("asd",("bsd","csd","ece"))
zval = [12,324,2.2]
"""

singularSequencesGood = """xvals = {k:v, k2:v2}
(singleAssignment,yvals) = ("asd",("bsd","csd","ece"))
zvals = [12,324,2.2]
"""

testCoverageGood = """
xVals=[]
yDictVals={}
def fun(x):
    pass
func([12,1,2])
"""

goodFileD = """
error_string = ('The bear ' + self.name +
                ' does not fulfill all requirements.')
superfluous_options = options.keys() - allowed_options
super_options = options.keys() - allowed_options
superflous_option = options.keys() - allowed_option
super_option = options.keys() - allowed_options
options['settings'] = {}
if len(use_raw_files) > 1:
    return ((), {}, {}, {})
val = getattr(self, member)
origin = origin or ''
a,b,c = 2,1,3
collected_files, file_globs_with_files = [], []
collected_files, file_globs_with_file = [], []
varA, varB = self.functionCall()
function_list = [fun1, fun2]
__all__ = {key:value}
mapping_dict = {from_var:to, from_another_var:to_another}
children = ['Tim', 'Tom', 'Tammy']
(xVal, yVal) = self.func()
data = {'key':'value'}
"""

badFileC = """
a,b,c = 2,[],3
"""

badFileD = """
option['settings'] = {}
"""

badFileE = """
collected_file, file_globs_with_files = [], []
"""

badFileF = """
self.setting = ['Setting 1', 'Setting 2']
"""

badFileF = """
self.setting = ['Setting 1', 'Setting 2']
"""

functionArgsTest = verify_local_bear(
                    PyPluralNamingBear,
                    valid_files=(goodFileA, goodFileB, goodFileC,
                                 singularSequencesGood, testCoverageGood,),
                    invalid_files=(badFileA, badFileB, singularSequencesBad,),
                    settings={'disable_default_list': True},
                    )

plural_vars_test = verify_local_bear(
                    PyPluralNamingBear,
                    valid_files=(goodFileD,),
                    invalid_files=(badFileC, badFileD, badFileE, badFileF,),
                    )
